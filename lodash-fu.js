var _ = require('lodash');

// Basically, ._chunk takes an array {array} as input and produces a new array of arrays which is such that all arrays are of size {size} but the last one.
// If the input does not have the 'length' property, the result is []
// If the input has the 'length' property, it is treated as if it were an array
// Thus, if a string, is passed as input, the results will be consistent
// If an arbitrary object is passed as input, all values in the produced arrays will be undefined, except those for which the indices have a value defined in the object.
// If size is not given, it is assumed to be 1.

console.log(_.chunk([1],3));
console.log(_.chunk([1,2],3));
console.log(_.chunk([1,2,3],3));
console.log(_.chunk([1,2,3,4],3));
console.log(_.chunk([1,2,3,4,5],3));

console.log(_.chunk([1,2,3]));

console.log(_.chunk({a:'hello'}));

console.log(_.chunk("abracadabra",2));
console.log(_.chunk({length:4,1:'youpi',3:'tada'}));

var a = [1,2,3];
var b = _.chunk(a);
console.log(a);
console.log(b);

// ._compact produces a new arrays with all falsey values removed. These are:
// false, 0, null, undefined, "", NaN
// in particular, {}, [], and "0" are not falseys

// A catch is therefore to try to filter an array of numbers, which are accidentally of type strings ;
// if the expected behaviour is to have all 0 or "0" removed, those that are strings will remain
// Another problem is that it may be desirable to remove all null/undefined numbers from an array, but then the number 0 would be removed too, which is not desirable in that case

// Therefore => use with caution

var a = [0,1,2,3];
var b = _.compact(a);
console.log(a);
console.log(b);


// ._concat takes as input a list of arrays and procuces a new array containing all elements of all arrays.
// Elements in the list that are not arrays are treated as singleton arrays. 
// Elements that are not arrays and have the 'length' property are treated as singleton arrays
// Arrays within the arrays are not flattened

var a = [];
var b = _.concat([],2,{length:2},"abc",[4]);
console.log(a);
console.log(b);

// Basically, Object.is is stricter than === which is stricter than ==
// The only difference between Object.is and === is that Object.is distinguishes +0 from -0 and considers NaN equal to itself,
// i.e. +0===-0, but !Object.is(+0,-0), and !(NaN===NaN), but Object.is(NaN,NaN)

// _.difference can be used to filter out values using SameValueZer0, which is like Object.is, but assumes +0 and -0 are identical
// One side effect is that -0 is converted to 0 in the process
// Some uses cases are the following:
// - filtering out 0s : it would be annoying to have to specify both 0, and -0 in the second argument every time,
//   so not distinguishing between 0 and -0 is a good thing here ;
//   beware of number that have been converted to strings though, if "0" is possible in the input, it should be specified in the filter along with 0
// - filtering out NaNs : to do that, comparing NaN to NaN must returns true, which requires using Object.is

console.log([0,-0,NaN]);
console.log(_.difference([0,-0,NaN],[NaN]));
console.log(_.difference([0,-0,NaN],[0]));
console.log(_.difference([0,-0,NaN],[-0]));
console.log(_.difference([0,-0,NaN,null,undefined,""],[null]));
console.log(_.difference([0,-0,"0","-0",NaN],[-0]));

// _.differenceBy is like _.difference, but values are first converted through an helper function on both sides,
// this can be used to forcefully convert string numbers to numbers using something like (x)=>(+x)

console.log(_.differenceBy([0,-0,"0",1,"a","b","c",NaN], [0,"a",NaN], function(x){return +x;}));
console.log(_.differenceBy([0,-0,"0",1,"a","b","c",NaN], [0,"a",NaN], function(x){if(isNaN(+x)){return x+"x";} return +x;}));
console.log(_.differenceBy([{user:'titou',name:'Titou'},{user:'tata',name:'Tata'}], [{user:'titou'}], 'user'));

// _.differenceWith is like _.difference, but values are compared for equality using a specific comparator
// the comparator should at least be symmetric

console.log(
	_.differenceWith(
		[{user:'titou',name:'Titou'},{user:'fifit',name:'Fifit'},{user:'tata',name:'Tata'}],
		[{user:'tata'}],
		function(a,b){return a.user.length==b.user.length}
	)
);

// _.drop produce a new array with the {n} first elements dropped ; works on any object that has a length property

console.log(_.drop([1,2,3,4,5],2));
console.log(_.drop("abcde",2));
console.log(_.drop({length:5},2));

// _.dropRight produce a new array with the {n} last elements dropped

console.log(_.dropRight([1,2,3,4,5],2));

// _.dropRightWhile drops until the function predicate returns true

console.log(_.dropRightWhile([1,2,3,4,5],function(x){return x>3;}));

// _.dropWhile drops until the function predicate returns true

console.log(_.dropWhile([1,2,3,4,5],function(x){return x<3;}));

// _.fill fills the array with the given value
// does not extend the array if specified length is greater than array length
// can only be used to fill a subset of the array
// does not work on strings, leaves them untouched
// mutates the array
// works on any object that has a length property (but strings)

var a = [1,2,8];
var b = _.fill(a, 7, 0, 8);
console.log(a);
console.log(b);
console.log(_.fill("abcdef", "a", 0, 3));
console.log(_.fill("abcdef", "a", 0, 3));
console.log(_.fill({length:7}, "a", 0, 3));

// _.findIndex
// works on strings and any object that has the length property

console.log(_.findIndex(["youpi","tada","fim","youpla","four"], function(x){return x.length==4}));
console.log(_.findIndex("youpi", function(x){return x=="u"}));

// _.findLastIndex

// _.flatten
// does not treat strings and objects with the 'length' property as arrays

console.log(_.flatten([1,[1,2],[[3,4],[5,6]],"youpi",{length:3}]));

// _.flattenDeep
// no protection against recursive arrays
// (console.log has protection against that case)

console.log(_.flattenDeep([1,[1,2],[[3,4],[5,6]],"youpi",{length:3}]));
var a = [1];
a[1]=a;
console.log(_.flatten(a));
console.log(_.flatten(_.flatten(a)));
//console.log(_.flattenDeep(a)); don't call it !!


console.log('End.');

