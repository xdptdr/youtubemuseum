var express = require('express');
var app = express();

app.use(express.static('public'));

app.get('/', function (req, res) {
   res.send('Hello World');
})

var host = '127.0.0.1';
var port = 44444;

var server = app.listen(port, function () {
   console.log("Example app listening at http://%s:%s", host, port)
})